from Blockchain import Blockchain
import json

from flask import Flask, jsonify, request
from uuid import uuid4

from textwrap import dedent

#Tworzenie węzła dla blockchaina
app = Flask(__name__)

#Przydzielanie losowego identyfikatora węzłowi z uuid4() (bez pauz)
wezel_id = str(uuid4()).replace('-', '')

blockchain = Blockchain()


#Endpoint kopania
@app.route('/kop', methods=['GET'])
def kop():
    ostatni_blok = blockchain.ostatni_blok
    ostatni_dowod = ostatni_blok['proof']
    dowod = blockchain.proof_of_work(ostatni_dowod)

    #Węzeł, który wykopie nowy blok  otrzyma jedną monetę

    blockchain.nowa_transakcja(nadawca="0", odbiorca=wezel_id, kwota=1)

    poprzedni_hash = blockchain.hash(ostatni_blok)
    blok = blockchain.nowy_blok(dowod, poprzedni_hash)

    odpowiedz = {
        'wiadomosc': "Nowy blok przyłączony do łańcucha",
        'numer': blok['numer'],
        'transakcje': blok['transakcje'],
        'proof': blok['proof'],
        'poprzedni_hash': blok['poprzedni_hash'],
    }

    return jsonify(odpowiedz), 200

#Endpoint tworzenia nowej transakcji
@app.route('/transakcje/nowa', methods=['POST'])
def nowa_transakcja():


    wartosci = request.get_json(force=True)

    #b = request.args.get("odbiorca")
    #c = request.args.get("kwota")
    #Sprawdzanie czy zostały wypełnione wszystkie potrzebne pola w POST
    #pola = ['nadawca', 'odbiorca', 'kwota']

    if not(request.get_json()):
        return f'{wartosci}'


    #Tworzenie nowej transakcji
    numer = blockchain.nowa_transakcja(wartosci['nadawca'], wartosci['odbiorca'], wartosci['kwota'])

    odpowiedz={'wiadomosc': f'Transakcja została dodana do bloku {numer}'}

    return jsonify(odpowiedz), 201


#Endpoint zwracania całego łańcucha
@app.route('/lancuch', methods=['GET'])
def zwroc_lancuch():
    informacje={
        'lancuch': blockchain.lancuch,
        'dlugosc': len(blockchain.lancuch),
    }
    return jsonify(informacje), 200

@app.route('/wezly/nowy', methods=['POST'])
def wezel_nowy():
    wartosci = request.get_json(force=True)

    wezly = wartosci.get('wezly')
    if wezly is None:
        return "Nie wczytano żadnego węzła", 400

    for wezel in wezly:
        blockchain.nowy_wezel(wezel)

    odpowiedz = {
        'Wiadomość': 'Dodano nowe węzły',
        'Ilość węzłów': list(blockchain.wezly),

    }
    return jsonify(odpowiedz), 201

@app.route('/wezly/rozwiaz', methods=['GET'])
def rozwiaz():
    zamieniono = blockchain.rozwiaz_konflikt()

    if zamieniono:
        odpowiedz = {
            'Wiadomość' : 'łańcuch został podmieniony',
            'Nowy łańcuch': blockchain.lancuch
        }
    else:
        odpowiedz = {
            'Wiadomość': 'Nie podmieniono łańcucha',
            'łańcuch': blockchain.chain
        }
    return jsonify(odpowiedz), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)